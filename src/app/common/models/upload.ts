export class Upload {
    
      $key: string;
      file:File;
      name:string;
      url:string;
      reconciliationkey : string;
      progress:number;
      feedback:string;
      createdAt: Date = new Date();
    
      constructor(file:File) {
        this.file = file;
      }
    }