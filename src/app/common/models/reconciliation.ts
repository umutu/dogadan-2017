import { Supplier } from "./supplier";
import { Period } from "./period";

export class reconciliation {
    constructor(
      private supplier :Supplier,
      private IsActive:boolean,
      private IsConfirm:number,
      private type :number,
      private subject:string,
      private period:Period,
      private RequestDate,
      private FeedbackDate,
      private residuary:string){}
}
