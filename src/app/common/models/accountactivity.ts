import { Supplier } from "./supplier";
import { activitytype } from "./activitytype";
import { type } from "./type";

export class accountactivity
{

    constructor(private supplier:Supplier,
                private activitytype : activitytype,
                private documantNo:string,
                private description : string,
                private documantDate : number , 
                private option:number,
                private dueDate:number,
                private type:type,
                private documantDebit:number ,
                private dueIn:number,
                private documantamount:number,
                private residuary:number ,
                private timestamp){}
    

}