import { FirebaseService } from './firebase.service';
import { Injectable } from '@angular/core';

@Injectable()
export class MailService {
    constructor(private _firebaseService: FirebaseService) { }
    editmail(key:string,address:string)
    {
        
       this._firebaseService.rootRef.child("SystemMail/"+key+"/address").set(address);
    }
    getsystemmail()
    {
       return this._firebaseService.rootRef.child("SystemMail");
    }
    
}