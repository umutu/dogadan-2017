import { Injectable } from "@angular/core";
import { FirebaseService } from "./firebase.service";
import { activitytype } from "../models/activitytype";
import { type } from "../models/type";


@Injectable()
export class TypeService {
    constructor(private _db: FirebaseService){}
    editType(key: string, name: string) {
        this._db.rootRef.child(`Types/${key}`).set({ name: name });
      }
      saveType(name:string)
      {
     
        let newtype = new type(name);
        this._db.rootRef.child("Types").push(newtype);
      }

      getAllTypes() {
        return this._db.rootRef.child("Types");
      }

}

