import { FirebaseService } from './firebase.service';
import { Injectable } from '@angular/core';
import { Period } from '../models/period';
import { helper } from '../helper/helper';

@Injectable()
export class PeriodService {
    _helper : helper;
    constructor(private _db: FirebaseService) { this._helper = new helper(); }
    createperiod(name:string,startdate:number,enddate:number)
    {
     
       let period = new Period(name,startdate,enddate)
     return  this._db.rootRef.child("Periods").push(period);
    }
    getall()
    {
        return  this._db.rootRef.child("Periods");
    }

    updateSingleProperty(key: string, propertyname: any, propertyvalue: any) {
       
        if (propertyname === "startDate" || propertyname === "endDate") {
     
          propertyvalue = this._helper.sdtdateToTimestamp(propertyvalue);
        }
        return this._db.rootRef.child(`Periods/${key}/${propertyname}`).set(propertyvalue);
      }
  
    
}