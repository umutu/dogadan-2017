import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { FirebaseService } from "./firebase.service";
import { reconciliation } from "../models/reconciliation";
import { Supplier } from "../models/supplier";
import { AmountService } from "./amount.service";
import { Period } from "../models/period";
import * as firebase from 'firebase';
import { Upload } from "../models/upload";

@Injectable()
export class ReconciliationService {
    reconciliation : reconciliation;
  constructor(private _db: FirebaseService,private _residuaryService:AmountService){}
  createnewreconciliation(guid:string,supplierkey:string,supplier:Supplier,type:number,subject:string,period:Period, residuaryofSelectedSupplier:string){
    this.reconciliation = new reconciliation(supplier,true,0,type,subject,period,firebase.database.ServerValue.TIMESTAMP,0,residuaryofSelectedSupplier);
   return   this._db.rootRef.child("Reconciliations").child(guid).child(supplierkey).set(this.reconciliation );
  }
  getbyId(key:string)
  {
    return this._db.rootRef.child("Reconciliations").child(key);
  }
  getresiduary(key:string,period:Period)
  {
    return this._db.rootRef.child("Residuaries").child(key);
  }
  close(key:string,supplierkey:string,isconfirm:number)
  {
     this._db.rootRef.child("Reconciliations/"+key+"/"+supplierkey+"/IsActive").set(false);
     this._db.rootRef.child("Reconciliations/"+key+"/"+supplierkey+"/IsConfirm").set(isconfirm);
     this._db.rootRef.child("Reconciliations/"+key+"/"+supplierkey+"/FeedbackDate").set(firebase.database.ServerValue.TIMESTAMP);

  }
  getall()
  {
    return this._db.rootRef.child("Reconciliations");
  }
}
