import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth'
import { AppModule } from "../../app.module";
import 'rxjs/add/operator/do';
import { AngularFireDatabaseModule, FirebaseListObservable, FirebaseObjectObservable, AngularFireDatabase } from "angularfire2/database";



@Injectable()
export class FirebaseService {

  isAuth: boolean;
  rootRef = this._database.database.ref();
  rootRefDb = this._database.database;
  constructor(private _angularFireAuth: AngularFireAuth, private _router: Router, private _database: AngularFireDatabase) { }



  isAuthenticated() {
    return this._angularFireAuth.authState
      .map(user => {
        if (user)
          return true;
        else
          return false;
      });
  }

  isAnonymous() {
    return this._angularFireAuth.authState
      .map(user => {
        if (user)
          return false;
        else
          return true;
      });
  }

  isEmailVerified() {
    return this._angularFireAuth.authState
      .map(user => {
        if (!user || user.emailVerified)
          return true;
        else
          return false;
      });
  }

  isEmailNotVerified() {
    return this._angularFireAuth.authState
      .map(user => {
        if (!user || user.emailVerified)
          return false;
        else
          return true;
      });
  }

  login(email: string, password: string) {
    return this._angularFireAuth.auth.signInWithEmailAndPassword(email, password)
      .then(authState => {
      })
      .catch(error => {
        return error;
      })
      ;
  }

  signUp(email: string, password: string) {
    this._angularFireAuth.auth.createUserWithEmailAndPassword(email, password)
      .then(authState => {
        authState.sendEmailVerification()
          .then(result => {
            this._router.navigate(["/session/lockscreen"]);

          })
          .catch(error => {
            console.log("Error:", error);
          })
          ;
      })
      .catch(error => {
        console.log("Error:", error);
      })
      ;
  }

  logout() {
    this._angularFireAuth.auth.signOut()
      .then(result => {
        this.isAuth = false;
      })
      .catch(error => {
        console.log("Error:", error);
      })
      ;
  }

  GetCurrentUser() {
    return this._angularFireAuth.auth.currentUser;
  }

  ReSendActivationMail() {
    this._angularFireAuth.auth.currentUser.sendEmailVerification();
    this.logout();
  }

  ResetPassword(email: string) {
    return this._angularFireAuth.auth.sendPasswordResetEmail(email);
  }

}
