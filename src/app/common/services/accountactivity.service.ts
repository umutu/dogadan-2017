import { Injectable } from "@angular/core";
import { FirebaseService } from "./firebase.service";
import { Supplier } from "../models/supplier";
import { activitytype } from "../models/activitytype";
import { type } from "../models/type";
import { accountactivity } from "../models/accountactivity";
import { AmountService } from "./amount.service";
import { helper } from "../helper/helper";
import { MdSnackBar } from "@angular/material";
import * as firebase from 'firebase';
import { Period } from "../models/period";



@Injectable()
export class AccountActivityService {
    activityAccount : accountactivity;
    _helper : helper;
    constructor(private _db: FirebaseService,private _amountService : AmountService,public snackBar: MdSnackBar){ this._helper = new helper();}

    createAccountActivity(
                supplierkey:string,
                supplier:Supplier,
                activitytype : activitytype,
                documantNo:string,
                description : string,
                documantDate : number,
                opsion:number,
                dueDate:number,
                type:type,
                documantDebit:number ,
                dueIn:number)
    {

        let documantamount = Number(documantDebit) - Number(dueIn);
        let currentAmount;
        this._amountService.getjustAmount(supplierkey).once("value",snap =>{
           currentAmount= Number(snap.val());
           if(currentAmount===null)
           currentAmount = 0.00;
           currentAmount = currentAmount + (documantamount);

           this._amountService.setAmount(supplierkey,currentAmount).then(()=>{

            this.activityAccount = new accountactivity(
                supplier,
                activitytype,
                documantNo,
                description,
                documantDate,
                opsion,
                dueDate,
                type,
                documantDebit,
                dueIn,
                documantamount,
                currentAmount,
                firebase.database.ServerValue.TIMESTAMP
                );



              this._db.rootRef.child("AccountActivities").child(supplierkey).push(this.activityAccount).then(()=>{
                this._helper.opensnackbar(this.snackBar,2000, "Activity was created!","Goto All Activities")})
                .catch((e) => this._helper.opensnackbar(this.snackBar,2000,e.message,"Error"));


         });
        });





    }
    getAllActivitybySupplier(key:string)
    {
     return  this._db.rootRef.child("AccountActivities/"+key)
    }
    getAllActivitybySupplierBeetwenDates(key:string,period:Period)
    {
     return  this._db.rootRef.child("AccountActivities/"+key).orderByChild('documantDate').startAt(period.startDate).endAt(period.endDate);
    }
}
