import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { FirebaseService } from "./firebase.service";
import { Supplier } from "../models/supplier";

@Injectable()
export class SupplierService {
  constructor(private _db: FirebaseService){}

createSupplier(_supplier:Supplier)
{
  console.log(_supplier);
  return this._db.rootRef.child("Suppliers").push(_supplier);

}
getAll()
{
  return this._db.rootRef.child("Suppliers");
}
getAllForUsing()
{
  return this._db.rootRef.child("Suppliers").orderByChild("isDeleted").equalTo("No");
}

updateSingleSupplierProperty(key: string, propertyname: any, propertyvalue: any) {
  return this._db.rootRef.child(`Suppliers/${key}/${propertyname}`).set(propertyvalue);
}
}
