import { Injectable } from "@angular/core";
import { FirebaseService } from "./firebase.service";
import { activitytype } from "../models/activitytype";


@Injectable()
export class ActivityTypeService {
    constructor(private _db: FirebaseService){}
    editType(key: string, name: string,code:string) {
        this._db.rootRef.child(`ActivityTypes/${key}`).set({ name: name,code : code });
      }
      saveType(name:string,code:string)
      {
     
        let newtype = new activitytype(name,code);
        this._db.rootRef.child("ActivityTypes").push(newtype);
      }

      getAllActivityTypes() {
        return this._db.rootRef.child("ActivityTypes");
      }

}

