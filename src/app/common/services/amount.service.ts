import { FirebaseService } from "./firebase.service";
import { Injectable } from "@angular/core";
import { Period } from "../models/period";

@Injectable()
export class AmountService {
    constructor(private _db: FirebaseService){}

    getAmount(supplierkey:string)
    {
    return this._db.rootRef.child("AccountActivities").child(supplierkey).orderByChild('documantDate').limitToLast(1);
    }
    getjustAmount(supplierkey:string)
    {
    return this._db.rootRef.child("Residuaries").child(supplierkey);
    }
    getAmountBetweenDates(supplierkey:string,period:Period)
    {
    return this._db.rootRef.child("AccountActivities").child(supplierkey).orderByChild('documantDate').startAt(period.startDate).endAt(period.endDate).limitToLast(1);
    }
    setAmount(supplierkey:string,amount:number)
    {
     return   this._db.rootRef.child("Residuaries").child(supplierkey).set(amount);
    }
}
