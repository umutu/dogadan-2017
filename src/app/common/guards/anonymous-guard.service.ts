import { GuardsCheckEnd } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { FirebaseService } from './../services/firebase.service';
import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';

//Guard for restricting the autheticated users from anonymous areas
@Injectable()
export class AnonymousGuardService implements CanActivate {

  constructor(private _firebaseService: FirebaseService, private _router: Router) { }

  canActivate() {
    let anonymousSubscriber = this._firebaseService.isAnonymous();

    anonymousSubscriber.subscribe(isAuth => {
      if (!isAuth)
        this._router.navigate([""]);
    });

    return anonymousSubscriber;
  }

}
