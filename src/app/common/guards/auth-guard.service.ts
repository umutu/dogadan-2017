import { FirebaseService } from './../services/firebase.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from "@angular/router";

//Guard for restricting the anonymous users from authenticated areas
@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private _firebaseService: FirebaseService, private _router: Router) { }

  canActivate() {
    let authenticationSubscriber = this._firebaseService.isAuthenticated();

    authenticationSubscriber.subscribe(isAuth => {
      if (!isAuth)
        this._router.navigate(["session/signin"]);
    });

    return authenticationSubscriber;
  }

}
