import { Observable } from 'rxjs/Rx';
import { CanActivate, Router, GuardsCheckEnd } from '@angular/router';
import { Injectable } from '@angular/core';
import { FirebaseService } from '../services/firebase.service';

//Guard for lockscreen component to restrict a non-verified user from entering the authenticated area
@Injectable()
export class VerifyGuardService implements CanActivate {

  constructor(private _firebaseService: FirebaseService, private _router: Router) { }

  canActivate() {
    let emailSubscriber = this._firebaseService.isEmailVerified();
    emailSubscriber.subscribe(isValid => {
      if (!isValid)
        this._router.navigate(["session/lockscreen"]);
    });
    return emailSubscriber;
  }

}
