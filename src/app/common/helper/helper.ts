import { MdSnackBar, MdSnackBarConfig } from "@angular/material";
import { Injectable } from "@angular/core";

@Injectable()
export class helper
{
  opensnackbar(_snackbar: MdSnackBar,_duration : number, _message:string,_buttontext:string)
  {
    const config = new MdSnackBarConfig();
    config.duration = 3000;
    config.extraClasses =  null;
    _snackbar.open(_message, _buttontext, config);

  }
 snapshotToArray(snapshot) {
    var returnArr = [];
    snapshot.forEach(function(childSnapshot) {
        var item = childSnapshot.val();
        item.key = childSnapshot.key;

        returnArr.push(item);
    });
    return returnArr;
}

snapshotToArray1(snapshot) {
  var returnArr = [];
  snapshot.forEach(function(childSnapshot) {
      var itemg = childSnapshot.val();
      var key = Object.keys(itemg)[0]
      var item= itemg[key];
      item.key = key;
      returnArr.push(item);
  });
  return returnArr;
}

snapshotToDoubleArray(snapshot) {
  let a = 0;
  var tableRowsWithIdtemp = [];
  snapshot.forEach(b => {
    tableRowsWithIdtemp[a] = new Array(2);
    tableRowsWithIdtemp[a][0] = b.key;
    tableRowsWithIdtemp[a][1] = b.val().name;
    a++;
    return null;
  });
  return tableRowsWithIdtemp;
}



public converttoDateString(timestamp:any)
{
 let datetemp: Date;
 datetemp = new Date(timestamp);
 var day = ("0" + datetemp.getDate() ).slice(-2);
 var month = ("0" + (datetemp.getMonth())).slice(-2);
 var today = (month)+"/"+(day)+"/"+datetemp.getFullYear() ;
 return today;
}

converttoDate(timestamp:any)
{ let datetemp: Date;
   datetemp = new Date(timestamp);
   var day = ("0" + datetemp.getDate() ).slice(-2);
   var month = ("0" + (datetemp.getMonth())).slice(-2);
   var today = datetemp.getFullYear()+"-"+(month)+"-"+(day) ;
   return today;
 }


getValue(key, array) {
 let arraytemp = array;
  for (var el in array) {
      if (array[el].key === key) {
        delete arraytemp[el].key;
          return arraytemp[el];
      }
  }
}

snapshotToArraySpecial(snapshot) {
  var returnArr = [];
  snapshot.forEach(function(childSnapshot) {
      var item = childSnapshot.val();
      let datetemp1: Date;
      datetemp1 = new Date(item.startDate);
      var day1 = ("0" + datetemp1.getDate() ).slice(-2);
      var month1 = ("0" + (datetemp1.getMonth())).slice(-2);
      var today1 = (month1)+"/"+(day1)+"/"+datetemp1.getFullYear() ;

      let datetemp2: Date;
      datetemp2 = new Date(item.endDate);
      var day2 = ("0" + datetemp2.getDate() ).slice(-2);
      var month2 = ("0" + (datetemp2.getMonth())).slice(-2);
      var today2 = (month2)+"/"+(day2)+"/"+datetemp2.getFullYear() ;
      item.startDate = today1;
      item.endDate = today2;
      item.key = childSnapshot.key;

      returnArr.push(item);
  });
  return returnArr;
}

newGuid() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
  });
}

sdtdateToTimestamp(date:string )
{
  var myDate;
  myDate=date.split("-");
  var newDate=myDate[1]+"/"+myDate[2]+"/"+myDate[0];
  return new Date(newDate).getTime();
}

}

