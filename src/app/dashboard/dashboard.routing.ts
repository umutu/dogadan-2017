import { VerifyGuardService } from './../common/guards/verify-guard.service';
import { AuthGuardService } from './../common/guards/auth-guard.service';
import { Routes } from '@angular/router';

import { DashboardComponent } from './dashboard.component';

export const DashboardRoutes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    canActivate: [AuthGuardService, VerifyGuardService],
    children: [

    ]
  }
];
