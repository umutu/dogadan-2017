import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MdIconModule, MdCardModule, MdButtonModule, MdListModule, MdProgressBarModule, MdMenuModule, MdInputModule, MdSelectModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

import { ChartsModule } from 'ng2-charts/ng2-charts';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutes } from './dashboard.routing';
import { ReconciliationService } from '../common/services/reconciliation.service';
import { AmountService } from '../common/services/amount.service';
import { PeriodService } from '../common/services/period.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DashboardRoutes),
    MdIconModule,
    MdCardModule,
    MdButtonModule,
    MdListModule,
    MdProgressBarModule,
    MdMenuModule,
    ChartsModule,
    NgxDatatableModule,
    FlexLayoutModule,
    MdInputModule,
    MdSelectModule
  ],
  providers: [ ReconciliationService,AmountService,PeriodService],
  declarations: [ DashboardComponent ]
})

export class DashboardModule {}
