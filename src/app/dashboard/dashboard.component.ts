import { Component } from '@angular/core';
import { FirebaseService } from '../common/services/firebase.service';
import { ReconciliationService } from '../common/services/reconciliation.service';
import { helper } from '../common/helper/helper';
import { PeriodService } from '../common/services/period.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  _helper : helper;
  rows = [];
  temp =[] ;
  periods : any[];
  totalcount :number;
  confirmcount:number=0;
  rejectcount:number=0;
  mailsendedcount:number=0;
  constructor(private _reconciliationService:ReconciliationService,private _periodService : PeriodService)
  {
    this._helper =new helper();
    this._reconciliationService.getall().on("value",snap=>{
      this.totalcount = snap.numChildren();
      this.temp =this._helper.snapshotToArray1(snap);
      this.rows = this.temp;
      this.rows.forEach(element => {
        if(element.IsConfirm === 0)
        this.mailsendedcount++;
        else if(element.IsConfirm === 1)
        this.confirmcount ++;
        else if(element.IsConfirm === 2)
        this.rejectcount ++;
      });
    });
    this._periodService.getall().once("value",snap=>{
      this.periods =this._helper.snapshotToArray(snap);
    });
  }

  timestamptodate(time:number)
  {
    if(time===0)
    return "---";
    var d = new Date(time);
    return d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear();
  }
  updateFilterName(event) {
         const val = event.target.value.toLowerCase();
         const temp = this.temp.filter(function(d) {
           let results;
               if(event.srcElement.id === "inpcode")
               results = d.supplier.code.toLowerCase().indexOf(val) !== -1 || !val;
               else if(event.srcElement.id === "inpsupplier")
               results = d.supplier.name.toLowerCase().indexOf(val) !== -1 || !val;
               else if(event.srcElement.id === "inpemail")
               results = d.supplier.email.toLowerCase().indexOf(val) !== -1 || !val;
               else if(event.srcElement.id === "inpsubject")
               results = d.subject.toLowerCase().indexOf(val) !== -1 || !val;
 
               return results
        });
          this.rows = temp;
       }
       updateFilterbystatus(event) {
        const val = event.value.toLowerCase();
        if(val !== "-1")
        {
      
          const temp = this.temp.filter(function(d) {
            let results;
                results = d.IsConfirm == val;
              
  
                return results
         });
           this.rows = temp;
        }
        else
        {
          this.rows =this.temp;
        }
        
      }
      updateFilterbyperiod(event) {
       
        const val = event.value.key;
        if(event.value !== "-1")
        {
        
          const temp = this.temp.filter(function(d) {
            let results;
                results = d.period.key == val;
              
  
                return results
         });
           this.rows = temp;
        }
        else
        { 
          this.rows =this.temp;
        }
        
      }



}
