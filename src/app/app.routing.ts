import { AuthGuardService } from './common/guards/auth-guard.service';
import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';

export const AppRoutes: Routes = [{
  path: '',
  component: AdminLayoutComponent,
  children: [{
    path: '',
    loadChildren: './dashboard/dashboard.module#DashboardModule',
  },
  {
    path: 'supplier',
    loadChildren: './supplier/supplier.module#SupplierModule'
  },
  {
    path: 'processes',
    loadChildren: './processes/processes.module#ProcessesModule'
  },

  {
    path: 'settings',
    loadChildren: './settings/settings.module#SettingsModule'
  }]
}, {
  path: '',
  component: AuthLayoutComponent,
  children: [{
    path: 'session',
    loadChildren: './session/session.module#SessionModule'
  },{
    path: 'reconciliation',
    loadChildren: './reconciliation/reconciliation.module#ReconciliationModule'
  },]
}, {
  path: '**',
  redirectTo: 'session/404'
}];
