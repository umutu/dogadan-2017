import { VerifyGuardService } from './common/guards/verify-guard.service';
import { AnonymousGuardService } from './common/guards/anonymous-guard.service';
import { AuthGuardService } from './common/guards/auth-guard.service';
import { firebaseConfig } from './common/configs/firebase-config';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseService } from './common/services/firebase.service';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import {
  MdSidenavModule,
  MdCardModule,
  MdMenuModule,
  MdCheckboxModule,
  MdIconModule,
  MdButtonModule,
  MdToolbarModule,
  MdTabsModule,
  MdListModule,
  MdSlideToggleModule,
  MdSelectModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { SharedModule } from './shared/shared.module';
import { AngularFireModule } from "angularfire2";
import { AngularFireDatabaseModule,FirebaseListObservable,FirebaseObjectObservable, AngularFireDatabase } from "angularfire2/database";
import { AngularDualListBoxModule } from 'angular-dual-listbox/index';
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    AngularFireModule.initializeApp(firebaseConfig),
    RouterModule.forRoot(AppRoutes),
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    MdSidenavModule,
    MdCardModule,
    MdMenuModule,
    MdCheckboxModule,
    MdIconModule,
    MdButtonModule,
    MdToolbarModule,
    MdTabsModule,
    MdListModule,
    MdSlideToggleModule,
    MdSelectModule,
    FlexLayoutModule,
    AngularDualListBoxModule,
    Ng2SmartTableModule
  ],
  providers: [
    FirebaseService,
    AngularFireAuth,
    AuthGuardService,
    AnonymousGuardService,
    VerifyGuardService,
    AngularFireDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private _firebaseService: FirebaseService) { }

}
