import { Routes } from '@angular/router';
import { AuthGuardService } from '../common/guards/auth-guard.service';
import { VerifyGuardService } from '../common/guards/verify-guard.service';
import { AddSupplierComponent } from './add-supplier/add-supplier.component';
import { ListComponent } from './list/list.component';


export  const SupplierRoutes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardService, VerifyGuardService],
    children: [{
      path: 'add-supplier',
      component: AddSupplierComponent
    },
    {
      path: 'list',
      component: ListComponent
    }
  ]
}];
