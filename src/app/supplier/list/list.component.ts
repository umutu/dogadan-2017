import { Component, OnInit } from '@angular/core';
import { helper } from '../../common/helper/helper';
import { MdSnackBar } from '@angular/material';
import { SupplierService } from '../../common/services/supplier.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  _helper : helper;
  datetemp:Date;
  editing = {};
  rows = [];
  temp =[] ;
  constructor(private _supplierService : SupplierService,public snackBar: MdSnackBar) {

    this._helper =new helper();
    this._supplierService.getAll().on('value',snap => {
      this.temp =this._helper.snapshotToArray(snap);
      this.rows = this.temp ;
    })
   }


   updateValue(event, cell, cellValue, row) {

        this._supplierService.updateSingleSupplierProperty(row.key,cell,event.target.value).then(()=>{
           this.editing[row.$$index + '-' + cell] = false;
           this._helper.opensnackbar(this.snackBar,2000,"Successfully updated","Success");
         }).catch((error)=>{
        console.log(error);
        this._helper.opensnackbar(this.snackBar,2000,error.message,"Error");
         });

      }

      updateFilterName(event) {
   
        const val = event.target.value.toLowerCase();
        const temp = this.temp.filter(function(d) {
          let results;
              if(event.srcElement.id === "inpname")
              results = d.name.toLowerCase().indexOf(val) !== -1 || !val;
              else if(event.srcElement.id === "inpcode")
              results = d.code.toLowerCase().indexOf(val) !== -1 || !val;
              else if(event.srcElement.id === "inpemail")
              results = d.email.toLowerCase().indexOf(val) !== -1 || !val;
              // else
              // results = d.isTrusted.toLowerCase().indexOf(val) !== -1 || !val;

              return results
       });
         this.rows = temp;
      }
      updateFilterbystatus(event){
        const val = event.value;
        if(event.value !== "All")
        {
        
          const temp = this.temp.filter(function(d) {
            let results;
                results = d.isTrusted == val;
              
  
                return results
         });
           this.rows = temp;
        }
        else
        { 
          this.rows =this.temp;
        }
        
      }
      updateFilterbystatusdeleted(event)
      {
        const val = event.value;
        if(event.value !== "All")
        {
        
          const temp = this.temp.filter(function(d) {
            let results;
                results = d.isDeleted == val;
              
  
                return results
         });
           this.rows = temp;
        }
        else
        { 
          this.rows =this.temp;
        }
      }

  ngOnInit() {

  }

}
