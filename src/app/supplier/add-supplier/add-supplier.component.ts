import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { MdSnackBar } from '@angular/material';
import { helper } from '../../common/helper/helper';
import { Timestamp } from 'rxjs';
import { Supplier } from '../../common/models/supplier';
import { SupplierService } from '../../common/services/supplier.service';


@Component({
  selector: 'app-add-supplier',
  templateUrl: './add-supplier.component.html',
  styleUrls: ['./add-supplier.component.scss']
})
export class AddSupplierComponent implements OnInit {
  public form: FormGroup;
  helper = new helper();
  supplier: Supplier;
  constructor(private fb: FormBuilder,public snackBar: MdSnackBar,private _supplierService:SupplierService ) {
    this.form = this.fb.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(5)])],
      code: [null, Validators.compose([Validators.required,  Validators.minLength(3)])],
      email: [null, Validators.compose([Validators.required])],
      isTrusted: [null, Validators.compose([Validators.required])]

    });

   }
   onClear()
   {
    this.form.reset();
   }

   onSubmit()
   {
     this.supplier = new Supplier(
     this.form.controls["name"].value,
     this.form.controls["code"].value,
     this.form.controls["email"].value,
     this.form.controls["isTrusted"].value,
     "No");

     this._supplierService.createSupplier(this.supplier).then(()=>{
     this.helper.opensnackbar(this.snackBar,2000, "Supplier was created!","Goto All Suppliers")})
     .catch((e) => this.helper.opensnackbar(this.snackBar,2000,e.message,"Error"));
   }

  ngOnInit() {
  }

}
