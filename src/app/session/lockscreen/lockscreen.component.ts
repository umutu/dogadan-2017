import { FirebaseService } from './../../common/services/firebase.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'app-lockscreen',
  templateUrl: './lockscreen.component.html',
  styleUrls: ['./lockscreen.component.scss']
})
export class LockscreenComponent implements OnInit {

  email: string;
  constructor(private _angularFireAuth: AngularFireAuth, private router: Router, private _firebaseService: FirebaseService) { }

  ngOnInit(): void {
  this.email =  this._angularFireAuth.auth.currentUser.email;
  }

  SendVerification() {
    this._firebaseService.ReSendActivationMail()
   }
  Close()
  {
   this._firebaseService.logout();
  }

}
