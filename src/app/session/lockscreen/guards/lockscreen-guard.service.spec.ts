import { TestBed, inject } from '@angular/core/testing';

import { LockscreenGuardService } from './lockscreen-guard.service';

describe('LockscreenGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LockscreenGuardService]
    });
  });

  it('should be created', inject([LockscreenGuardService], (service: LockscreenGuardService) => {
    expect(service).toBeTruthy();
  }));
});
