import { Injectable } from '@angular/core';
import { FirebaseService } from '../../../common/services/firebase.service';
import { Router, CanActivate } from '@angular/router';

//Guard for lockscreen component to restrict a verified user from entering the component area
@Injectable()
export class LockscreenGuardService implements CanActivate {

  constructor(private _firebaseService: FirebaseService, private _router: Router) { }

  canActivate() {
    let emailSubscriber = this._firebaseService.isEmailNotVerified();
    emailSubscriber.subscribe(isValid => {
      if (!isValid)
        this._router.navigate([""]);
    });
    return emailSubscriber;
  }

}
