import { AppModule } from './../../app.module';
import { Observable } from 'rxjs/Observable';
import { FirebaseService } from './../../common/services/firebase.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  public form: FormGroup;
  loginError : any;

  constructor(private fb: FormBuilder, private _router: Router, private _firebaseService: FirebaseService) { }

  ngOnInit() {
    this.form = this.fb.group({
      uname: [null, Validators.compose([Validators.required])], password: [null, Validators.compose([Validators.required])]
    });
  }

  signIn() {
    this._firebaseService.login(this.form.controls["uname"].value, this.form.controls["password"].value).then( a=>{
      this.loginError=a;
    });
  }

}
