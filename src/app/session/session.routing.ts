import { LockscreenGuardService } from './lockscreen/guards/lockscreen-guard.service';
import { AuthGuardService } from './../common/guards/auth-guard.service';
import { AnonymousGuardService } from './../common/guards/anonymous-guard.service';
import { Routes } from '@angular/router';

import { NotFoundComponent } from './not-found/not-found.component';
import { ErrorComponent } from './error/error.component';
import { ForgotComponent } from './forgot/forgot.component';
import { LockscreenComponent } from './lockscreen/lockscreen.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';

export const SessionRoutes: Routes = [
  {
    path: '',
    children: [{
      path: '404',
      component: NotFoundComponent,
      canActivate: [AnonymousGuardService]
    }, {
      path: 'error',
      component: ErrorComponent,
      canActivate: [AnonymousGuardService]
    }, {
      path: 'forgot',
      component: ForgotComponent,
      canActivate: [AnonymousGuardService]
    }, {
      path: 'lockscreen',
      component: LockscreenComponent,
      canActivate: [AuthGuardService, LockscreenGuardService]
    }, {
      path: 'signin',
      component: SigninComponent,
      canActivate: [AnonymousGuardService]
    }, {
      path: 'signup',
      component: SignupComponent,
      canActivate: [AnonymousGuardService]
    }]
  }
];
