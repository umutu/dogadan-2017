import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { FirebaseService } from '../../common/services/firebase.service';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss']
})
export class ForgotComponent implements OnInit {

  public form: FormGroup;
  resetInfo : any;
  resetStatus = false;
  constructor(private fb: FormBuilder, private _router: Router  , private _firebaseService: FirebaseService) {this.resetStatus=false;}

  ngOnInit() {
    this.form = this.fb.group ( {
      email: [ null, Validators.compose( [ Validators.required, CustomValidators.email ] ) ]
    } );
  }

  onSubmit() {
   this._firebaseService.ResetPassword(this.form.controls["email"].value).then((result)=>{
    this.resetStatus=true;
    this.resetInfo = "Reset password email was sent!";
        setTimeout(() => {
          this._router.navigate([""]);
      }, 3000);
    }).catch((error)=>{
      this.resetStatus=true;
      this.resetInfo = error.message;
   });
  }

}
