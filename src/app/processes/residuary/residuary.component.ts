import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { SupplierService } from '../../common/services/supplier.service';
import { helper } from '../../common/helper/helper';
import { AmountService } from '../../common/services/amount.service';
import { HttpClient } from '@angular/common/http';
import { RequestOptions, RequestMethod } from '@angular/http';
import { ReconciliationService } from '../../common/services/reconciliation.service';
import { MdSnackBar } from '@angular/material';
import { Supplier } from '../../common/models/supplier';
import { PeriodService } from '../../common/services/period.service';
import { Period } from '../../common/models/period';

@Component({
  selector: 'app-residuary',
  templateUrl: './residuary.component.html',
  styleUrls: ['./residuary.component.scss']
})
export class ResiduaryComponent implements OnInit {
  tdSuppliers: any[];
  tdPeriods: any[];
  _helper : helper;
  residuaryofSelectedSupplier :string = "0.00 TL";
  seledtedSupplier :string;
  seledtedPeriod :Period;
  subject:string;
  supplieremail : string;
  supplier : Supplier;
  type :number;
  datetemp : Date;
  constructor(private _periodService : PeriodService,private _supplierService : SupplierService,public snackBar: MdSnackBar, private _amountService : AmountService,private _reconciliationService:ReconciliationService ,private http: HttpClient) {  this._helper = new helper();

    this._supplierService.getAllForUsing().on('value',snap => {
      this.tdSuppliers =this._helper.snapshotToArray(snap);
    });

    this._periodService.getall().once('value',snap =>{
      this.tdPeriods =this._helper.snapshotToArray(snap);
    });
  }

  ngOnInit() {
  }
  onChange(event)
  {

     this._amountService.getAmount(event.value.key).on("value",snap =>{
       this.supplier = event.value;
       let currentvalue;
       if(snap.val() === null)
       currentvalue=0.00;
       else
       currentvalue = Object.values(snap.val()).map((v) => v.residuary)[0];
       if(currentvalue < 0) this.type=1;
       else this.type=0;
       this.supplieremail = event.value.email;
       this.residuaryofSelectedSupplier =currentvalue  + " TL";
       this.seledtedSupplier = event.value.key;
     });
  }

  onPeriodChange(event)
  {
   this._amountService.getAmountBetweenDates(this.seledtedSupplier,event.value).once("value",snap =>
  {
    this.seledtedPeriod = event.value;
    let currentvalue;
    if(snap.val() === null)
    currentvalue=0.00;
    else
    currentvalue = Object.values(snap.val()).map((v) => v.residuary)[0];
    if(currentvalue < 0) this.type=1;
    else this.type=0;
    this.supplieremail = event.value.email;
    this.residuaryofSelectedSupplier =currentvalue  + " TL";
  })
  }


  onClick()
  {
    let newguid = this._helper.newGuid();
    const body = {
                 guid: newguid,
                 towho : this.supplieremail,
                 subject : this.subject
                }



    this.http.get("http://www.etimesgut7noluasm.com/3d97aadbb2e94fcdbb650f9b94d44071/email/mail.php?guid="+body.guid+"&towho="+body.towho).subscribe(
      data => { this._reconciliationService.createnewreconciliation(newguid,this.seledtedSupplier ,this.supplier,this.type,this.subject,this.seledtedPeriod, this.residuaryofSelectedSupplier).then(()=>{
        this._helper.opensnackbar(this.snackBar,2000, "Mail was sended!","")})
        .catch((e) => this._helper.opensnackbar(this.snackBar,2000,e.message,"Error")); },
      error => console.log(error)
  );


  }
  converttoDateString(timestamp:string)
  {

   this.datetemp = new Date(timestamp);
   var day = this.datetemp.getDate();
   var month = this.datetemp.getMonth()+1;
   var year =  this.datetemp.getFullYear();
   var today = day+"/"+month+"/"+year ;
   return today;
 }

}
