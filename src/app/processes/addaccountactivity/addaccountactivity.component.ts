import { Component, ViewChild } from '@angular/core';
import { FormControl, NgModel, FormGroup, FormBuilder, Validators } from '@angular/forms';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import { SupplierService } from '../../common/services/supplier.service';
import { helper } from '../../common/helper/helper';
import { ActivityTypeService } from '../../common/services/activityType.service';
import { TypeService } from '../../common/services/type.service';
import { accountactivity } from '../../common/models/accountactivity';
import { MdSnackBar } from '@angular/material';
import { AccountActivityService } from '../../common/services/accountactivity.service';


@Component({
  selector: 'app-addaccountactivity',
  templateUrl: './addaccountactivity.component.html',
  styleUrls: ['./addaccountactivity.component.scss']
})
export class AddaccountactivityComponent  {

  _helper : helper;
  stateCtrl: FormControl;
  currentState = '';
  topHeightCtrl = new FormControl(0);

  reactiveStates: any;
  reactiveAuto;
  tdSuppliers: any[];
  tdAuto;
  options = [ ];

  public form: FormGroup;
  activityAccount : accountactivity;
  currenActivityType : any;
  activitytypes : any[];
  types : any[];

  @ViewChild("NgAutoComp") modelDir: NgModel;

  constructor(private _supplierService : SupplierService,private _activitytypeService:ActivityTypeService,private _typeService:TypeService,private fb: FormBuilder,public snackBar: MdSnackBar,private _activityaccountservice:AccountActivityService) {
  
    this.form = this.fb.group({
      supplier: [null,Validators.compose([Validators.required])],
      activityType: [null,Validators.compose([Validators.required])],
      documantNo : [null,Validators.compose([Validators.required])],
      documantDate : [null,Validators.compose([Validators.required])],
      opsion : [null,Validators.compose([Validators.required])],
      dueDate :[null,Validators.compose([Validators.required])],
      type: [null,Validators.compose([Validators.required])],
      documantDebit:[null,Validators.compose([Validators.required])],
      dueIn:[null,Validators.compose([Validators.required])],
      description : [null,Validators.compose([Validators.required])]


    });



  this._helper = new helper();
    this._supplierService.getAllForUsing().on('value',snap => {
      this.tdSuppliers =this._helper.snapshotToArray(snap);
    });

    this._activitytypeService.getAllActivityTypes().on('value',snap => {
      this.activitytypes =this._helper.snapshotToArray(snap);


    });
    
    for (var index = 0; index < 100; index++) {
      this.options.push({ value: index+'', viewValue: index+''});
      
    }    

    this._typeService.getAllTypes().on('value',snap => {
      this.types =this._helper.snapshotToArray(snap);
    });

  }
  onSubmit()
  {
    
  this._activityaccountservice.createAccountActivity(
                                              this.form.controls["supplier"].value.key,
                                              this.form.controls["supplier"].value,
                                              this.form.controls["activityType"].value,
                                              this.form.controls["documantNo"].value,
                                              this.form.controls["description"].value,
                                              this._helper.sdtdateToTimestamp(this.form.controls["documantDate"].value),
                                              this.form.controls["opsion"].value,
                                              this._helper.sdtdateToTimestamp(this.form.controls["dueDate"].value),
                                              this.form.controls["type"].value,
                                              this.form.controls["documantDebit"].value,
                                              this.form.controls["dueIn"].value
                                              )
  }
  keyPress(event)
  {  const pattern = /[0-9\+\.\ ]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }

  }
  


}
