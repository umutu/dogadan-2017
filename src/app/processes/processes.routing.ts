import { Routes } from '@angular/router';
import { AuthGuardService } from '../common/guards/auth-guard.service';
import { VerifyGuardService } from '../common/guards/verify-guard.service';
import { AddaccountactivityComponent } from './addaccountactivity/addaccountactivity.component';
import { ListaccountactivityComponent } from './listaccountactivity/listaccountactivity.component';
import { ResiduaryComponent } from './residuary/residuary.component';


export  const ProcessesRoutes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardService, VerifyGuardService],
    children: [{
      path: 'app-addaccountactivity',
      component: AddaccountactivityComponent
    },
    {
      path: 'app-residuary',
      component: ResiduaryComponent
    }

  ]
}];
