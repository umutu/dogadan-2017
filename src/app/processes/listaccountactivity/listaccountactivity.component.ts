import { Component, OnInit, Input } from '@angular/core';
import { helper } from '../../common/helper/helper';
import { AccountActivityService } from '../../common/services/accountactivity.service';

@Component({
  selector: 'app-listaccountactivity',
  templateUrl: './listaccountactivity.component.html',
  styleUrls: ['./listaccountactivity.component.scss']
})
export class ListaccountactivityComponent implements OnInit {
@Input() seledtedSupplier:any;
@Input() seledtedPeriod:any;
  rows = [];
  _helper : helper;
  datetemp :Date;
  constructor(private _accountActivityService:AccountActivityService) { this._helper =new helper(); }
  ngOnInit() {
  }

  ngOnChanges()
  {
   
    if(typeof(this.seledtedSupplier) !== typeof(undefined))
    {
       if(typeof(this.seledtedPeriod) === typeof(undefined))
       {
        this._accountActivityService.getAllActivitybySupplier(this.seledtedSupplier).once('value',snap => {
          this.rows =this._helper.snapshotToArray(snap);

        })
       }
       else
       {

        this._accountActivityService.getAllActivitybySupplierBeetwenDates(this.seledtedSupplier,this.seledtedPeriod).once('value',snap => {
          this.rows =this._helper.snapshotToArray(snap);

        })
       }


    }



  }
  converttoDateString(timestamp:string)
  {
    this.datetemp = new Date(timestamp);
    var day = this.datetemp.getDate();
    var month = this.datetemp.getMonth()+1;
    var year =  this.datetemp.getFullYear();
    var today = day+"/"+month+"/"+year ;
    return today;
 }


}
