import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddaccountactivityComponent } from './addaccountactivity/addaccountactivity.component';
import { ListaccountactivityComponent } from './listaccountactivity/listaccountactivity.component';
import { ResiduaryComponent } from './residuary/residuary.component';
import { RouterModule } from '@angular/router';
import { ProcessesRoutes } from './processes.routing';
import { MdButtonModule, MdAutocompleteModule, MdButtonToggleModule, MdCardModule, MdCheckboxModule, MdChipsModule, MdTableModule, MdDatepickerModule, MdDialogModule, MdExpansionModule, MdGridListModule, MdIconModule, MdInputModule, MdListModule, MdMenuModule, MdPaginatorModule, MdProgressBarModule, MdProgressSpinnerModule, MdRadioModule, MdRippleModule, MdSelectModule, MdSidenavModule, MdSlideToggleModule, MdSliderModule, MdSnackBarModule, MdSortModule, MdTabsModule, MdToolbarModule, MdTooltipModule, MdNativeDateModule, StyleModule, MdSelectionModule } from '@angular/material';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CdkTableModule } from '@angular/cdk';
import { AngularDualListBoxModule } from 'angular-dual-listbox';
import { SupplierService } from '../common/services/supplier.service';
import { ActivityTypeService } from '../common/services/activityType.service';
import { TypeService } from '../common/services/type.service';
import { AccountActivityService } from '../common/services/accountactivity.service';
import { AmountService } from '../common/services/amount.service';
import { ReconciliationService } from '../common/services/reconciliation.service';
import { PeriodService } from '../common/services/period.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProcessesRoutes),
    MdAutocompleteModule,
    MdButtonModule,
    MdButtonToggleModule,
    MdCardModule,
    MdCheckboxModule,
    MdChipsModule,
    MdTableModule,
    MdDatepickerModule,
    MdDialogModule,
    MdExpansionModule,
    MdGridListModule,
    MdIconModule,
    MdInputModule,
    MdListModule,
    MdMenuModule,
    MdPaginatorModule,
    MdProgressBarModule,
    MdProgressSpinnerModule,
    MdRadioModule,
    MdRippleModule,
    MdSelectModule,
    MdSidenavModule,
    MdSlideToggleModule,
    MdSliderModule,
    MdSnackBarModule,
    MdSortModule,
    MdTabsModule,
    MdToolbarModule,
    MdTooltipModule,
    MdNativeDateModule,
    StyleModule,
    NgxDatatableModule,
    MdSelectionModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    CdkTableModule,
    AngularDualListBoxModule
  ],
  providers : [
    SupplierService,
    ActivityTypeService,
    TypeService,
    AccountActivityService,
    AmountService,
    ReconciliationService,
    PeriodService
     ],
  declarations: [AddaccountactivityComponent, ListaccountactivityComponent, ResiduaryComponent]
})
export class ProcessesModule { }
