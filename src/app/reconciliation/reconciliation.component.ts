import { Component, OnInit, ViewChild } from '@angular/core';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { ReconciliationService } from '../common/services/reconciliation.service';
import { helper } from '../common/helper/helper';
import { AccountActivityService } from '../common/services/accountactivity.service';
import { Upload } from '../common/models/upload';
import { UploadService } from '../common/services/upload.service';
import { reconciliation } from '../common/models/reconciliation';
import { HttpClient } from '@angular/common/http';
import { MdSnackBar } from '@angular/material';
import { MailService } from '../common/services/mail.service';
import { Period } from '../common/models/period';
import { AmountService } from '../common/services/amount.service';
import { Angular2Csv } from 'angular2-csv';
import { TableColumn, DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-reconciliation',
  templateUrl: './reconciliation.component.html',
  styleUrls: ['./reconciliation.component.scss']
})
export class ReconciliationComponent implements OnInit {
 InfoMessage :string="Request was closed! Thanks for your feedback!!";
 supplierName:string;
 residuary:string = "loading...";
 period:Period;
 supplierkey:string;
 isdetailsvisible:boolean=false;
 isreject:boolean=false;
 rows = [];
 _helper : helper;
 selectedFiles: FileList;
 currentUpload: Upload;
 currentguid:string;
 feedback:string;
 reconciliation:reconciliation;
 Isclosed:boolean;
 Isloaded:boolean=false;

 datetemp : Date;
  constructor(private http: HttpClient,private _mailService : MailService, private uploadService: UploadService ,public snackBar: MdSnackBar, private route: ActivatedRoute,private _reconciliationService : ReconciliationService ,private _accountActivityService:AccountActivityService,private _amountService:AmountService) {this._helper =new helper();
   }
   @ViewChild('dataTable')  dataTable: DatatableComponent;
  ngOnInit() {
    this.route
    .queryParams
    .subscribe(params => {
      this.currentguid = params['guid'];
        this._reconciliationService.getbyId(this.currentguid).once("value",snap=>{
          this.Isloaded=true;
        if(snap.val()===null)
        {
          this.Isclosed=true;
          this.InfoMessage="Some Error occurred!";
        }

        else
        {
          if(!Object.values(snap.val()).map((v) => v.IsActive)[0])
          {
            this.Isclosed=true;

          }
          else
          {

            this.Isclosed=false;
            this.supplierName = Object.values(snap.val()).map((v) => v.supplier.name)[0];
            this.supplierkey = Object.values(snap.val()).map((v) => v.supplier.key)[0];
            this.period = Object.values(snap.val()).map((v) => v.period)[0];
            this.residuary = Object.values(snap.val()).map((v) => v.residuary)[0];

          }
        }



        });

    });
  }
  onClickDetails()
  {
   this.isdetailsvisible = true;
    this._accountActivityService.getAllActivitybySupplierBeetwenDates(this.supplierkey,this.period).once('value',snap => {

      this.rows =this._helper.snapshotToArray(snap);

    })
  }

  onClickRejectWithDocument()
  {
    this.isreject = true;
  }

  onClickReject()
  {
    this.closereconciliation(2);
  }
  oncloseClick()
  {
    this.isreject = false;
  }
  detectFiles(event) {
    this.selectedFiles = event.target.files;
}
   uploadFile() {
  let file = this.selectedFiles.item(0);
  this.currentUpload = new Upload(file);
  this.uploadService.pushUpload(this.currentUpload,this.currentguid,this.feedback,this.supplierkey);
  this.closereconciliation(2);
}

closereconciliation(isconfirm:number)
{
  this._reconciliationService.close(this.currentguid,this.supplierkey,isconfirm);
  this.Isclosed=true;

  this._mailService.getsystemmail().once("value",snap =>{

     const body = {
      towho: Object.values(snap.val()).map((v) => v.address)[0],
      suppliername : this.supplierName
     }
     this.http.get("http://www.etimesgut7noluasm.com/3d97aadbb2e94fcdbb650f9b94d44071/email/mailme.php?suppliername="+body.suppliername+"&towho="+body.towho).subscribe(
      data =>{},
      error => console.log(error)
      );
  });



}
onClickConfirm()
{
  this.closereconciliation(1);
}
converttoDateString(timestamp:string)
{
 this.datetemp = new Date(timestamp);
 var day = ("0" + this.datetemp.getDate() ).slice(-2);
 var month = ("0" + (this.datetemp.getMonth())).slice(-2);
 var today = (month)+"/"+(day)+"/"+this.datetemp.getFullYear() ;
 return today;
}

exportAsCSV() {
  const columns: TableColumn[] = this.dataTable.columns 
  const headers =
      columns
          .map((column: TableColumn) => column.name)
          .filter((e) => e);  // remove column without name (i.e. falsy value)

  const rows: any[] = this.dataTable.rows.map((row) => {
      let r = {};
      columns.forEach((column) => {
          if (!column.name) { return; }   // ignore column without name
          if (column.prop) {
              let prop = column.prop;
              r[prop] = (typeof row[prop] === 'boolean') ? (row[prop]) ? 'Yes'
                                                                       : 'No'
                                                         : row[prop];
          } else {
              // special cases handled here
          }
      })
      return r;
  });

  const options = {
      fieldSeparator  : ' --',
      quoteStrings    : '"',
      decimalseparator: '.',
      showLabels      : true,
      headers         : headers,
      showTitle       : false,
      title           : 'Report',
      useBom          : true,
  };
  return new Angular2Csv(rows, 'report', options);
}
}

