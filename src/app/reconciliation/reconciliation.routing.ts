import { AuthGuardService } from "../common/guards/auth-guard.service";
import { VerifyGuardService } from "../common/guards/verify-guard.service";
import { Routes } from "@angular/router";
import { ReconciliationComponent } from "./reconciliation.component";


export const ReconciliationRoutes: Routes = [
  {
    path: '',
    component: ReconciliationComponent

}];
