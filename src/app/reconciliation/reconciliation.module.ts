import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReconciliationComponent } from './reconciliation.component';
import { RouterModule } from '@angular/router';
import { ReconciliationRoutes } from './reconciliation.routing';
import { MdCardModule, MdButtonModule, MdButtonToggleModule, MdSelectionModule, StyleModule, MdNativeDateModule, MdTooltipModule, MdToolbarModule, MdTabsModule, MdSortModule, MdSnackBarModule, MdSliderModule, MdSlideToggleModule, MdSidenavModule, MdSelectModule, MdRippleModule, MdRadioModule, MdProgressSpinnerModule, MdProgressBarModule, MdPaginatorModule, MdMenuModule, MdListModule, MdInputModule, MdIconModule, MdGridListModule, MdExpansionModule, MdDialogModule, MdDatepickerModule, MdTableModule, MdChipsModule, MdCheckboxModule, MdAutocompleteModule } from '@angular/material';
import { ReconciliationService } from '../common/services/reconciliation.service';
import { AmountService } from '../common/services/amount.service';
import { CdkTableModule } from '@angular/cdk';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AccountActivityService } from '../common/services/accountactivity.service';
import { UploadService } from '../common/services/upload.service';
import { MailService } from '../common/services/mail.service';
import { Angular2Csv } from 'angular2-csv';

@NgModule({
  imports: [
    CommonModule ,
    RouterModule.forChild(ReconciliationRoutes),
    MdAutocompleteModule,
    MdButtonModule,
    MdButtonToggleModule,
    MdCardModule,
    MdCheckboxModule,
    MdChipsModule,
    MdTableModule,
    MdDatepickerModule,
    MdDialogModule,
    MdExpansionModule,
    MdGridListModule,
    MdIconModule,
    MdInputModule,
    MdListModule,
    MdMenuModule,
    MdPaginatorModule,
    MdProgressBarModule,
    MdProgressSpinnerModule,
    MdRadioModule,
    MdRippleModule,
    MdSelectModule,
    MdSidenavModule,
    MdSlideToggleModule,
    MdSliderModule,
    MdSnackBarModule,
    MdSortModule,
    MdTabsModule,
    MdToolbarModule,
    MdTooltipModule,
    MdNativeDateModule,
    StyleModule,
    NgxDatatableModule,
    MdSelectionModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    CdkTableModule
  ],
  providers: [ReconciliationService,AmountService,AccountActivityService,UploadService,MailService],
  declarations: [ReconciliationComponent]
})
export class ReconciliationModule { }
