import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { helper } from '../../common/helper/helper';
import { CustomValidators } from 'ng2-validation';
import { MdSnackBar } from '@angular/material';
import { PeriodService } from '../../common/services/period.service';
import { DatatableComponent, TableColumn } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-period',
  templateUrl: './period.component.html',
  styleUrls: ['./period.component.scss']
})
export class PeriodComponent implements OnInit {

  editing = {};
  datetemp:Date;
  public form: FormGroup;
  _helper = new helper();
  rows = [];
  temp =[] ;
  constructor(private fb: FormBuilder,public snackBar: MdSnackBar,private _periodService:PeriodService ) {
    this._helper =new helper();
    this._periodService.getall().on('value',snap => {
      this.temp =this._helper.snapshotToArray(snap);
      this.rows = this.temp ;
    })


  }

  ngOnInit() {

    this.form = this.fb.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(5)])],
      startDate: [null, Validators.compose([Validators.required, CustomValidators.date])],
      endDate: [null, Validators.compose([Validators.required, CustomValidators.date])],


    });
  }

  onSubmit()
  {
  
    this._periodService.createperiod(this.form.controls["name"].value,
    this._helper.sdtdateToTimestamp(this.form.controls["startDate"].value),
    this._helper.sdtdateToTimestamp(this.form.controls["endDate"].value)).
    then(()=>{
      this._helper.opensnackbar(this.snackBar,2000, "Period was created!","")
    this.form.reset();

}).catch((e) => this._helper.opensnackbar(this.snackBar,2000,e.message,"Error"))
  }



  updateValue(event, cell, cellValue, row) {
 
        this._periodService.updateSingleProperty(row.key,cell,event.target.value).then(()=>{
           this.editing[row.$$index + '-' + cell] = false;
           this._helper.opensnackbar(this.snackBar,2000,"Successfully updated","Success");
         }).catch((error)=>{
        console.log(error);
        this._helper.opensnackbar(this.snackBar,2000,error.message,"Error");
         });

      }


      converttoDateString(timestamp:string)
      {
        this.datetemp = new Date(timestamp);
        var day = this.datetemp.getDate();
        var month = this.datetemp.getMonth()+1;
        var year =  this.datetemp.getFullYear();
        var today = day+"/"+month+"/"+year ;

        return today;
     }
      converttoDate(timestamp:string)
     {

     this._helper.sdtdateToTimestamp

     this.datetemp = new Date(timestamp);
     var day = ("0" + this.datetemp.getDate() ).slice(-2);
     var month = ("0" + (this.datetemp.getMonth()+1)).slice(-2);
     var year =  this.datetemp.getFullYear();
     var today = year+"-"+(month)+"-"+(day) ;


      return today;
      }

      error:any={isError:false,errorMessage:''};
      
      compareTwoDates(){
         if(new Date(this.form.controls['endDate'].value)<new Date(this.form.controls['startDate'].value)){
            this.error={isError:true,errorMessage:'End cant before start'};
         }
      }
   
    }
