import { Component, OnInit } from '@angular/core';
import { helper } from '../../common/helper/helper';
import { LocalDataSource } from 'ng2-smart-table';
import { MailService } from '../../common/services/mail.service';

@Component({
  selector: 'app-mail',
  templateUrl: './mail.component.html',
  styleUrls: ['./mail.component.scss']
})
export class MailComponent implements OnInit {
  _helper : helper;

  settingsTypes = {
    columns: {
      address: {
        title: 'Address'
      }

    },
    confirmCreate :true,
    pager:
    {
      perPage : 1
    },
    add:{
      confirmCreate :true
    },
    edit: {
      confirmSave: true,
    },
    actions: {
      add:false,
      delete:false
      }

  };


  sourceB: LocalDataSource;

  constructor(private _mailService : MailService) {
    this.sourceB = new LocalDataSource();
     this._helper = new helper();
     this._mailService.getsystemmail().once("value",snap =>{

      this.sourceB.load(this._helper.snapshotToArray(snap));
     });
  }

  editMail(event)
  {

    this._mailService.editmail(event.newData.key,event.newData.address);
    event.confirm.resolve(event.newData);

  }

  ngOnInit() {
  }

}
