import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { helper } from '../../../common/helper/helper';
import { TypeService } from '../../../common/services/type.service';

@Component({
  selector: 'app-type',
  templateUrl: './type.component.html',
  styleUrls: ['./type.component.scss']
})
export class TypeComponent  {

  _helper : helper;
  settingsTypes = {
    columns: {
     name: {
        title: 'Name'
      }

    },
    confirmCreate :true,
    pager:
    {
      perPage : 15
    },
    add:{
      confirmCreate :true
    },
    edit: {
      confirmSave: true,
    },
    actions: {
    
      delete:false
      }

  };
  sourceA: LocalDataSource;
  constructor(private _typeService:TypeService){
    this.sourceA = new LocalDataSource();
    this._helper = new helper();
  }
  loadAdata()
  {
    this.sourceA= new LocalDataSource();;
    this._typeService.getAllTypes().on("value", snap => {
      this.sourceA.load(this._helper.snapshotToArray(snap));
       });
  }

  ngOnInit() {
    this.loadAdata();
  }

  createAType(event)
  {
 
    this._typeService.saveType(event.newData.name);
    event.confirm.resolve(event.newData);
    this.loadAdata();

  }


  editAType(event)
  {

    this._typeService.editType(event.newData.key,event.newData.name);
    event.confirm.resolve(event.newData);

  }

}
