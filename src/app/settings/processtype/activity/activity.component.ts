import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { helper } from '../../../common/helper/helper';
import { ActivityTypeService } from '../../../common/services/activityType.service';


@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit {

  _helper : helper;
  settingsAccountActivations = {
    columns: {
     name: {
        title: 'Name'
      },
      code: {
        title: 'Code'
      }

    },
    confirmCreate :true,
    pager:
    {
      perPage : 15
    },
    add:{
      confirmCreate :true
    },
    edit: {
      confirmSave: true,
    },
    actions: {
    
      delete:false
      }

  };
  source: LocalDataSource;
  constructor(private _activityTypeService:ActivityTypeService){
    this.source = new LocalDataSource();
    this._helper = new helper();
  }


  loaddata()
  {
    this.source= new LocalDataSource();;
    this._activityTypeService.getAllActivityTypes().on("value", snap => {
      this.source.load(this._helper.snapshotToArray(snap));
       });
  }

  ngOnInit() {
    this.loaddata();
  }

  createType(event)
  {
 
    this._activityTypeService.saveType(event.newData.name,event.newData.code);
    event.confirm.resolve(event.newData);
    this.loaddata();

  }


  editType(event)
  {

    this._activityTypeService.editType(event.newData.key,event.newData.name,event.newData.code);
    event.confirm.resolve(event.newData);

  }
}
