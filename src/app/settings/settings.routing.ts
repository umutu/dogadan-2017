import { AuthGuardService } from "../common/guards/auth-guard.service";
import { VerifyGuardService } from "../common/guards/verify-guard.service";
import { Routes } from "@angular/router";
import { ActivityComponent } from "./processtype/activity/activity.component";
import { TypeComponent } from "./processtype/type/type.component";
import { MailComponent } from "./mail/mail.component";
import { PeriodComponent } from "./period/period.component";


export const SettingsRoutes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardService, VerifyGuardService],
    children: [{
      path: 'app-activity',
      component: ActivityComponent
    },
    {
      path: 'app-type',
      component: TypeComponent
    },
    {
      path: 'app-mail',
      component: MailComponent
    },
    {
      path: 'app-period',
      component: PeriodComponent
    }

  ]
}];
