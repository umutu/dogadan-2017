import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SettingsRoutes } from './settings.routing';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { MdCardModule, MdInputModule, MdSnackBarModule, MdAutocompleteModule, MdButtonModule, MdButtonToggleModule, MdCheckboxModule, MdChipsModule, MdTableModule, MdDatepickerModule, MdDialogModule, MdExpansionModule, MdGridListModule, MdIconModule, MdListModule, MdMenuModule, MdPaginatorModule, MdProgressBarModule, MdRadioModule, MdRippleModule, MdSelectModule, MdSidenavModule, MdSlideToggleModule, MdSliderModule, MdSortModule, MdTabsModule, MdToolbarModule, MdTooltipModule, MdNativeDateModule, StyleModule, MdSelectionModule, } from '@angular/material';
import { ActivityTypeService } from '../common/services/activityType.service';
import { TypeComponent } from './processtype/type/type.component';
import { ActivityComponent } from './processtype/activity/activity.component';
import { TypeService } from '../common/services/type.service';
import { MailComponent } from './mail/mail.component';
import { MailService } from '../common/services/mail.service';
import { PeriodComponent } from './period/period.component';
import { PeriodService } from '../common/services/period.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { HttpModule } from '@angular/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CdkTableModule } from '@angular/cdk';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SettingsRoutes),
    Ng2SmartTableModule,
    MdCardModule,
    MdInputModule,
    FormsModule,
    ReactiveFormsModule,
    MdSnackBarModule,
    NgxDatatableModule,
    MdAutocompleteModule,
    MdButtonModule,
    MdButtonToggleModule,
    MdCardModule,
    MdCheckboxModule,
    MdChipsModule,
    MdTableModule,
    MdDatepickerModule,
    MdDialogModule,
    MdExpansionModule,
    MdGridListModule,
    MdIconModule,
    MdInputModule,
    MdListModule,
    MdMenuModule,
    MdPaginatorModule,
    MdProgressBarModule,
    MdRadioModule,
    MdRippleModule,
    MdSelectModule,
    MdSidenavModule,
    MdSlideToggleModule,
    MdSliderModule,
    MdSnackBarModule,
    MdSortModule,
    MdTabsModule,
    MdToolbarModule,
    MdTooltipModule,
    MdNativeDateModule,
    StyleModule,
    MdNativeDateModule,
    NgxDatatableModule,
    MdSelectionModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    CdkTableModule
  ],
  providers:
  [
    ActivityTypeService,TypeService,MailService,PeriodService
  ],

  declarations: [ TypeComponent, ActivityComponent, MailComponent, PeriodComponent]
})
export class SettingsModule { }
