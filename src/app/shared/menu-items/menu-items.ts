import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

const MENUITEMS = [
  {
    state: 'dashboard',
    name: 'DASHBOARD ',
    type: 'link',
    icon: 'explore'
  },
  {
    state: 'supplier',
    name: 'SUPPLIER',
    type: 'sub',
    icon: 'apps',
    children: [
      {state: 'add-supplier', name: 'New'},
      {state: 'list', name: 'List'}
    ]
  },

  {
    state: 'processes',
    name: 'PROCESSES',
    type: 'sub',
    icon: 'apps',
    badge: [

    ],
    children: [
      {state: 'app-addaccountactivity', name: 'Account'},
      {state: 'app-residuary', name: 'Residuary'}

    ]
  },
  {
    state: 'settings',
    name: 'SETTINGS',
    type: 'sub',
    icon: 'settings_applications',
    children: [
      {state: 'app-activity', name: 'Activity'},
      {state: 'app-type', name: 'Type'},
      {state: 'app-mail', name: 'Mail'},
      {state: 'app-period', name: 'Period'}

    ]


  }



];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  add(menu: Menu) {
    MENUITEMS.push(menu);
  }
}
